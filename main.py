from flask import *
import json, time, cv2, pytesseract, urllib, numpy as np, re

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def ocr_core(filename):
    url_response = urllib.request.urlopen(filename)
    img_array = np.array(bytearray(url_response.read()), dtype=np.uint8)
    img = cv2.imdecode(img_array, -1)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # img = cv2.medianBlur(img, 5)
    # img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    text = pytesseract.image_to_string(img)
    return text

# Fetch string using regex
def fetch_price(text):
    regex = r"(?:\b(?:RM|MYR))\s?(?:\d{1,3}(?:,\d{3})*|\d+)(?:\.\d{1,2})?(?!\.?\d)"
    matches = re.search(u'{regex}', text)
    return matches

def fetch_time(text):
    regex = r"(\d{1,2}:\d{2} [AP]M)"
    matches = re.match(regex, text)
    return matches

app = Flask(__name__)

@app.route('/ocr-request', methods=['POST'])
def ocr_request_page():
    id = request.form['id']
    image = ocr_core(request.form['image'])
    price = fetch_price(image)
    date = fetch_time(image)
    # image = ocr_core('https://pbs.twimg.com/media/DwtNhKLUUAAZ3OV.jpg')
    data_set = {'Order ID': f'{id}', 'Image Data':f'{image}', 'price':f'{price}', 'time': f'{date}'} 
    json_dump = json.dumps(data_set)

    return json_dump

if __name__ == '__main__':
    app.run(port=7777, debug=True)